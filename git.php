<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * Standalone command line file to update packages from git
 *
 * usage:
 * 
 * 		php -f git.php forms
 * 		download last version of package "forms" in the current directory
 * 
 *      php -f git.php ovidentia
 * 		download last version of package "ovidentia" in the current directory and 
 *      the last version before x.x.90 (patch branch)
 * 
 *      php -f git.php ovidentia 8.4.90
 * 		download "ovidentia-8-4-90.zip" in the current directory
 * 
 * 		ls * | php -f git.php
 * 		add last version of packages in current directory if newer version exists
 */






/**
 * 
 *
 */
class storeclient_git
{
	const REPO_PATTERN = 'https://bitbucket.org/cantico/%s';
	
	/**
	 * Fetch tags from remote location
	 * @param string $module
	 */
	public function getTags($module)
	{
		$tags = $this->execCmd('git ls-remote --tags '.sprintf(self::REPO_PATTERN, $module));
		$list = array();
		
		// first preg_match for deprecated versions tags (CVS compatibles)
        if (preg_match_all('|refs/tags/version-([\d\-]+)\n|', $tags, $matches)) {
            foreach ($matches[1] as $str) {
                $version = str_replace('-', '.', $str);
                $list[$version] = 'version-'.$str;
            }
        }
        
        if (preg_match_all('|refs/tags/([\d\.]+)\n|', $tags, $matches)) {
            foreach ($matches[1] as $str) {
                $list[$str] = $str;
            }
        }
		
		return $list;
	}

	
	
	/**
	 * Execute system command
	 * @param string $cmd
	 * @return object changelog_return
	 */
	protected function execCmd($cmd)
	{
		$handle = popen($cmd, 'r');
		if (false === $handle) {
			throw new Exception("No access rights to system command execution");
		}
		$buffer = '';
		while (!feof($handle)) {
			$buffer .= fgets($handle, 1024);
		}
		pclose($handle);
		return $buffer;
	}
	
	
	/**
	 * Get list of tags
	 * 
	 */
	public function getSortedTags($module)
	{
		$list = $this->getTags($module);
		uksort($list, 'version_compare');
		
		return $list;
	}
	
	
	/**
	 * 
	 * @param string $checkoutFile		destination file
	 * @param string $module	Module name
	 */
	protected function downloadArchive($checkoutFile, $modulename, $tag)
	{
		$gitrepo = sprintf(self::REPO_PATTERN, strtolower($modulename));
		
		$tmp_dir = sprintf('/tmp/storeclient_%s', uniqid());
		$modulelocation = $tmp_dir.'/'.$modulename;
		
		mkdir($tmp_dir);
		
		echo "$modulename $tag\n";
		
		// clone git repository and checkout requested version
		
		$this->execCmd(
		    sprintf(
		        'cd "%s" && git clone "%s" "%s" && cd "%s" && git checkout -q %s',
    			$tmp_dir,
    			$gitrepo,
    			$modulename,
		        $modulename,
		        $tag
            )
        );
		
		if ('ovidentia' === $modulename) {
		    $this->execCmd(
		        sprintf(
		            'cd "%s" && rm -Rf tests/ && composer install --no-dev --prefer-dist --ignore-platform-reqs --no-autoloader',
		            $modulelocation
	            )
	        );
		} else {
		    $this->execCmd(
	            sprintf(
    	            'cd "%s" && rm -Rf tests/ && cd programs/ && composer install --no-dev --prefer-dist --ignore-platform-reqs',
    	            $modulelocation
	            )
	        );
		}
		
		// create archive

		$this->execCmd(
		    sprintf(
		        'cd "%s" && zip -r %s * && cd .. && rm -Rf %s',
		        $modulelocation,
		        $checkoutFile,
		        $modulename
	        )
	    );
		
		if (file_exists($checkoutFile)) {
			echo "$checkoutFile\n";
		}
		
		$this->deldir($tmp_dir);
	}
	
	/**
	 * Get full apth to the output file
	 * @param string $dest sestination folder
	 * @param string $modulename
	 * @param string $version
	 * 
	 * @return string
	 */
	protected function getCheckoutFile($dest, $modulename, $version)
	{
	    $filename = sprintf('%s-%s.zip', $modulename, str_replace('.', '-', $version));
	    
	    $checkoutFile = $dest.'/'.$filename;
	    
	    if (file_exists($checkoutFile)) {
	        throw new Exception(sprintf("%s allready exists", $filename));
	    }
	    
	    return $checkoutFile;
	}
	
	
	/**
	 * 
	 * @param string $dest destination folder
	 * @param string $modulename
	 */
	public function downloadLastArchive($dest, $modulename)
	{
		$arr = $this->getSortedTags($modulename);
		
		if (empty($arr)) {
			throw new Exception(sprintf("%s has no version tags", $modulename));
		}
		
		$tag = end($arr);
		$version = key($arr);
		
		try {
		    $this->downloadArchive($this->getCheckoutFile($dest, $modulename, $version), $modulename, $tag);
		} catch(Exception $e) {
		    echo $e->getMessage()."\n";
		}
		
		if ($modulename !== 'ovidentia') {
		    return;
		}
		
		// if this is ovidentia, we also get the last from the PATCH branch
		
		$versions = array_keys($arr);
		
		do {
		    $version = array_pop($versions);
		    $d = explode('.', $version);
		} while (isset($d[2]) && 90 <= (int) $d[2]);
		
		$tag = $arr[$version];
		
		try {
		    $this->downloadArchive($this->getCheckoutFile($dest, $modulename, $version), $modulename, $tag);
		} catch(Exception $e) {
		    echo $e->getMessage()."\n";
		}
	}
	
	
	
	/**
	 * Download the last version of a module in current folder
	 * @param string $modulename
	 * @param string [$tag]
	 */
	public function update($modulename, $tag = null)
	{
		$dest = realpath(dirname('.'));
		try {
		    
		    if (isset($tag)) {
		        return $this->downloadArchive($this->getCheckoutFile($dest, $modulename, $tag), $modulename, $tag);
		    }
		    
			$this->downloadLastArchive($dest, $modulename);
		} catch (Exception $e) {
			echo $e->getMessage()."\n";
		}
	}
	
	
	
	/**
	 * Delete a folder recursively
	 * return true on success
	 * @param	string	$dir		folder to delete
	 * @return	bool
	 */
	protected function deldir($dir)
	{
		if (!file_exists($dir)) {
			throw new Exception(sprintf('The folder does not exists : %s', $dir));
		}
	
	
		$current_dir = opendir($dir);
		while ($entryname = readdir($current_dir)) {
			if (is_dir("$dir/$entryname") and ($entryname != "." and $entryname!="..")) {
				if (false === $this->deldir($dir.'/'.$entryname)) {
					return false;
				}
			} elseif ($entryname != "." and $entryname!="..") {
				if (false === unlink($dir.'/'.$entryname)) {
					throw new Exception(sprintf('The file is not deletable : %s', $dir.'/'.$entryname));
				}
			}
		}
		closedir($current_dir);
		if (false === rmdir($dir)) {
			throw new Exception(sprintf('The folder is not deletable : %s', $dir));
		}
		return true;
	}
	
	/**
	 * Process module name
	 */
	protected function mn($modulename)
	{
		$modulename = trim($modulename);
		$matches = null;
		
		if (preg_match('/^([^\-]+)-[\w\-]+\.zip$/', $modulename, $matches)) {
			$modulename = $matches[1];
		}
		
		return $modulename;
	}
	
	/**
	 * Update module given as argument
	 */
	public function updateArgModule()
	{
		if (isset($_SERVER["argv"][1])) {
		    
		    $tag = null;
		    if (isset($_SERVER["argv"][2])) {
		        $tag = $_SERVER["argv"][2];
		    }
		    
			$this->update($this->mn($_SERVER["argv"][1]), $tag);
		}
	}
	
	
	/**
	 * Update modules given with a pipe
	 * 
	 * @return string
	 */
	public function updateStdinModules()
	{
		$duplicates = array();
		
		stream_set_blocking(STDIN, 0);
		while ($modulename = fgets(STDIN)) {
			$modulename = $this->mn($modulename);
			
			if (isset($duplicates[$modulename])) {
				continue; // ignore duplicates module names
			}
			
			$this->update($modulename);
			$duplicates[$modulename] = 1;
		}
		
		return $modulename;
	}
}






$git = new storeclient_git();
$git->updateArgModule();
$git->updateStdinModules();
