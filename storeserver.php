<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * standalone script to serve addons in a csv file
 * used by the storeclient ovidentia addon
 */

ini_set('display_errors', 0);
error_reporting(E_ALL);
define('STORESERVER_ROOT', '.');
define('STORESERVER_CACHE', 'storeserver.cache');
define('STORESERVER_DATA_FOLDER', 'data');


class storeserver
{
    /**
     * @var array
     */
    private $addons;



    private static function getRootPath()
    {
        return realpath(STORESERVER_ROOT);
    }

    /**
     * Return the path to the data folder associated to the specific addon zip.
     * If the folder does not exists it is created.
     *
     * @param ZipArchive $zip
     * @return string
     */
    private static function getZipAddonDataFolder(ZipArchive $zip)
    {
        $dataFolder = self::getRootPath() . '/' . STORESERVER_DATA_FOLDER . '/' . basename($zip->filename);
        if (!file_exists($dataFolder)) {
            mkdir($dataFolder, 0777, true);
        }
        return $dataFolder;
    }


    /**
     * Extract a file form the addon zip and place it in the addon data folder.
     *
     * @param ZipArchive $zip
     * @param string $filePath      The file to extract
     * @return string   The path of the extracted file.
     */
    private function extractAddonFile(ZipArchive $zip, $filePath)
    {
        $dataFolder = self::getZipAddonDataFolder($zip);
        $destPath = $dataFolder . '/' . basename($filePath);
        $content = $zip->getFromName($filePath);
        file_put_contents($destPath, $content);
        return $destPath;
    }



    private function convertEncoding($str)
    {
        if ('UTF-8' == $this->iniEncoding) {
            return $str;
        }
        return mb_convert_encoding($str, 'UTF-8', $this->iniEncoding);
    }

    /**
     *
     * @param ZipArchive $zip
     * @return array|false
     */
    private function extractZipAddonInfo(ZipArchive $zip)
    {
        $languages = array(
            'en' => '',
            'fr' => '.fr'
        );

        $ini = $zip->getFromName('programs/addonini.php');

        if (!$ini) {
            // not an addon

            $ini = $zip->getFromName('ovidentia/version.inc');

            if (!$ini) {
                // not ovidentia version
                return false;
            }
        }

        $conf = parse_ini_string($ini, true);

        if (!$conf) {
            return false;
        }

        $general = $conf['general'];
        $this->iniEncoding = isset($general['encoding']) ? $general['encoding'] : 'ISO-8859-15';

        if (isset($conf['addons'])) {
            $dependencies = array();
            foreach ($conf['addons'] as $name => $v) {
                $f = substr($v, 0, 1);
                if ('>' != $f && '<' != $f && '=' != $f) {
                    $v = '>='.$v;
                }
                $dependencies[$name] = $v;
            }

        } else {
            $dependencies = array();
        }

        if (isset($general['ov_version'])) {
            $dependencies['ovidentia'] = '>='.$general['ov_version'];
        }

        $name = $this->convertEncoding($general['name']);
        $relativePath = substr($zip->filename, strlen(self::getRootPath()));
        $version = $this->convertEncoding($general['version']);
        $license = isset($general['license']) ? $this->convertEncoding($general['license']) : null;
        $date = date("Y-m-d H:i:s", filectime($zip->filename));
        $description = null;
        $descriptions = array();
        foreach ($languages as $language => $extension) {
            $key = 'description' . $extension;
            if (isset($general[$key])) {
                $descriptions[$language] = $this->convertEncoding($general[$key]);
                if (!isset($description)) {
                    $description = $descriptions[$language];
                }
            }
        }


        $addon = array(
            'name' => $name,
            'relativePath' => $relativePath,
            'version' => $version,
            'date' => $date,
            'license' => $license,
            'description' => $description,
            'descriptions' => $descriptions,
            'dependencies' => $dependencies
        );

        if (isset($general['tags'])) {
            $addon['tags'] = preg_split('/\s*,\s*/', $general['tags']);
        }

        if (isset($general['icon'])) {
            $destPath = $this->extractAddonFile($zip, 'skins/ovidentia/images/' . $general['icon']);
            $addon['icon'] = substr($destPath, strlen(self::getRootPath()));
        }
        if (isset($general['image'])) {
            $destPath = $this->extractAddonFile($zip, 'skins/ovidentia/images/' . $general['image']);
            $addon['image'] = substr($destPath, strlen(self::getRootPath()));
        }

        $addon['longDescription'] = null;
        foreach ($languages as $language => $extension) {
            $key = 'long_description' . $extension;
            if (isset($general[$key])) {
                $destPath = $this->extractAddonFile($zip, $general[$key]);

                if (substr($destPath, -3) === '.md') {
                    require_once dirname(__FILE__).'/Parsedown.php';
                    $Parsedown = new Parsedown();
                    $md = file_get_contents($destPath);

                    $html = $Parsedown->text($md);
                    $destPath = substr($destPath, 0, strlen($destPath) - 3) . '.html';
                    file_put_contents($destPath, $html);
                }

                $addon['longDescriptions'][$language] = substr($destPath, strlen(self::getRootPath()));
                if (!isset($addon['longDescription'])) {
                    $addon['longDescription'] = $addon['longDescriptions'][$language];
                }
            }
        }


        return $addon;
    }


    /**
     *
     * @param string $path  The addon zip file.
     */
    private function getAddonInfo($path)
    {
        $ext = strrchr($path, '.');

        if ('.zip' !== $ext) {
            return;
        }

        if (!is_readable($path)) {
            return false;
        }

        $zip = new ZipArchive();
        if (false === $zip->open($path)) {
            return false;
        }

        $addon = $this->extractZipAddonInfo($zip);
        if ($addon !== false) {
            $addonName = $addon['name'];
            if (!isset($this->addons[$addonName])) {
                $this->addons[$addonName] = array();
            }

            $this->addons[$addonName][] = $addon;
        }

        $zip->close();
    }



    /**
     * Output rows found in the given directory
     * @param string $path
     */
    private function getlist($path)
    {
        $files = scandir($path);
        foreach ($files as $value) {
            if ($value === '.' || $value === '..') {
                continue;
            }

            if (is_dir("$path/$value")) {
                $this->getlist("$path/$value");
            } else {
                $this->getAddonInfo("$path/$value");
            }
        }
    }


    /**
     * Get last update timestamp.
     * @param string $path
     * @return int  The time is returned as a Unix timestamp.
     */
    private function getLastUpdate($path)
    {
        $files = scandir($path);
        $lastUpdate = 0;

        foreach ($files as $value) {
            if ($value === '.' || $value === '..') {
                continue;
            }

            if (is_dir("$path/$value")) {
                $ts = $this->getLastUpdate("$path/$value");
            } else {
                $ts = filemtime("$path/$value");
            }

            if ($lastUpdate < $ts) {
                $lastUpdate = $ts;
            }
        }

        return $lastUpdate;
    }


    /**
     * @return string
     */
    public function getJson()
    {
        if (!isset($this->addons)) {
            $this->addons = array();
            $this->getlist(STORESERVER_ROOT);
        }

        return json_encode($this->addons);
    }


    public function getCached()
    {
        $cache = STORESERVER_ROOT.'/'.STORESERVER_CACHE;
        if (!file_exists($cache) || filemtime($cache) < $this->getLastUpdate(STORESERVER_ROOT)) {
            $data = $this->getJson();
            file_put_contents($cache, $data);
        } else {
            $data = file_get_contents($cache);
        }

        echo $data;
    }


}


$storeserver = new storeserver;

header('Content-type:application/json; charset=utf-8');

//echo $storeserver->getJson();
echo $storeserver->getCached();
