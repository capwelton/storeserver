# Mise en place du serveur #

Créer le fichier index.php qui est un lien symbolique vers /home/store/storeserver/storeserver.php dans le dossier partagé par apache avec le nom d'hote store.ovidentia.org.

structure des dossiers

    /home/store/storeclient
    ├──storeserver.php
    ├──git.php

    /var/www/store
    ├──git_autoupdate/
    ├──addons/
    ├──index.php


Pour mettre a jour automatiquement les modules du dossier git_autoupdate sur le store, utiliser cette tâche planifiée :

    30 * * * * cd /var/www/store/git_autoupdate && ls * | /usr/bin/php -f /home/store/storeserver/git.php

Une recherche de nouvelles mises à jour est faite toute les 30 minutes


le dossier /home/store/storeserver peut être mis a jour avec "git pull"